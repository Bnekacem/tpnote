package ent.ensim.data;

import static org.junit.Assert.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;


public class AdministrateurTest {
	  private static final Logger logger = LogManager.getLogger(AdministrateurTest.class.getName());

	
	Entreprise e;
    Agence a1, a2;
    Empruntable e1, e2, e3, e4, e5, e6;
    Employe admin, emp1, emp2, emp3, emp4;

	@Before
	public void setUp() {
		e = new Entreprise("Super Entreprise de l'ENSIM");
        a1 = new Agence("Agence du Sud");
        a2 = new Agence("Agence du Nord");

        e.getAgence().add(a1);
        e.getAgence().add(a2);

       // e1 = new Uc(true);
        e2 = new Ecran();
        e3 = new Souris();
        e4 = new Clavier();
        e5 = new Clavier();
        e6 = new Ecran();
        admin = new Adiminstrateur(a1, e);
        emp1 = new Employe(a1, e);
        emp2 = new Employe(a1, e);
        emp3 = new Employe(a2, e);
        emp4 = new Employe(a2, e);

        a1.getEmployes().add(admin);
        a1.getEmployes().add(emp1);
        a1.getEmployes().add(emp2);

        a2.getEmployes().add(emp3);
        a2.getEmployes().add(emp4);

	}
	@Test
    public void attribuerMateriel() {

        int stockActuel = admin.listeMateriel().size();
        ((Adiminstrateur) admin).AttribuerMaterielEmploye(new Ecran(), admin);

        Assert.assertEquals("attribuerMateriel: fail", stockActuel+1, admin.listeMateriel().size());
    }
	 @Test
	    public void recupererMateriel() {

	        int stockActuel = admin.listeMateriel().size();
	        emp1.listeMateriel().add(e1);
	        ((Adiminstrateur) admin).recupererMateriel(e1, emp1);

	        Assert.assertEquals("recupererMateriel: fail", stockActuel+1, admin.listeMateriel().size());
	    }
	 
	 @Test
	    public void stockEntreprise() {
	        e.listeMateriel().add(e1);
	        int stockEntreprise = ((Adiminstrateur) admin).StockEntreprise().size();
	        Assert.assertEquals("stock entreprise: fail", 1, stockEntreprise);
	    }
	 
	 @Test
	    public void supprimerMaterielDefectueuxAgence_sansMaterielDefectueux() {
	        a1.listeMateriel().add(e4);
	        a1.listeMateriel().add(e5);

	        int stockA1 = a1.listeMateriel().size();
	        ((Adiminstrateur) admin).supprimerMaterielAgence();
	        Assert.assertEquals("supprimerMaterielDefectueuxAgence_sansMaterielDefectueux: fail", stockA1, a1.listeMateriel().size());
	    }


	 @Test
	    public void supprimerMaterielDefectueuxEntreprise_avecMaterielDefectueux() {
	        e.listeMateriel().add(e4);
	        e.listeMateriel().add(e5);
	        e5.declarerDefectueux();

	        int stockE = e.listeMateriel().size();
	        ((Adiminstrateur) admin).supprimerMaterielEntreprise();
	        Assert.assertEquals("supprimerMaterielDefectueuxEntreprise_avecMaterielDefectueux: fail", stockE-1, e.listeMateriel().size());
	        for (Empruntable e : e.listeMateriel()) {
	            Assert.assertEquals("supprimerMaterielDefectueuxEntreprise_avecMaterielDefectueux: fail car il reste un Empruntable defectueux", false, e.isDefectuex());
	        }
	    }
	 
	 
	 

	@Test
	public void test() {
	}

}
