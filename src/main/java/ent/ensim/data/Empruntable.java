package ent.ensim.data;

public class Empruntable {

	
	protected boolean limitationPretAuxAgence;
	protected boolean defectueux ;
	
	
	public boolean getLimitationPretAuxAgence() {
		return limitationPretAuxAgence;
	}
	
	
	public void setLimitationPretAuxAgence(boolean limitationPretAuxAgence) {
		this.limitationPretAuxAgence = limitationPretAuxAgence;
	}
	
	
	public boolean getDefectueux() {
		return defectueux;
	}
	public void setDefectueux(boolean defectueux) {
		this.defectueux = defectueux;
	}
	
	
	public void declarerDefectueux() {
		
	}
	
	public boolean isDefectuex() {
		return defectueux;
		
	}
	
	
	
}
