package ent.ensim.data;

import java.util.List;

public class Adiminstrateur extends Employe{

	private Agence agence ;
	private Entreprise entreprise ;
	
	

	public Agence getAgence() {
		return agence;
	}
	public void setAgence(Agence agence) {
		this.agence = agence;
	}
	public Entreprise getEntreprise() {
		return entreprise;
	}
	public void setEntreprise(Entreprise entreprise) {
		this.entreprise = entreprise;
	}
	
	
	
	
	
	public Adiminstrateur(List<Empruntable> stock, Agence agence, Entreprise entreprise, Agence agence2,
			Entreprise entreprise2) {
		super(stock, agence, entreprise);
		agence = agence2;
		entreprise = entreprise2;
	}
	public Adiminstrateur( Agence agence, Entreprise entreprise) {
		super( agence, entreprise);
		agence = agence;
		entreprise = entreprise;
	}
	public boolean recupererMateriel(Empruntable e,Emprunteur em) {
 
	
		   em.getStock().remove(e);
			return true ;

		
	}

	public boolean AttribuerMaterielEmploye(Empruntable e,Emprunteur em) {
		
		em.ajouterAuStock(e);
		return true ;
		}
	
	public List<Empruntable>Stockagence(){
		
			return this.agence.stock;

		
	}
	public List<Empruntable> StockEntreprise (){
		return this.entreprise.stock;
			
		}
		
		
	public void supprimerMaterielEntreprise() {
		for (Empruntable c : this.entreprise.stock) {
			if(c.getDefectueux()==true) {
				this.entreprise.stock.remove(c);
			}
		}
		
		
	}
	public void supprimerMaterielAgence() {
		for (Empruntable c : this.agence.stock) {
			if(c.getDefectueux()==true) {
				this.agence.stock.remove(c);
			}
		}
		
		
	}
	
	
	
}
