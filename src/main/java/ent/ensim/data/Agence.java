package ent.ensim.data;

import java.util.ArrayList;
import java.util.List;

public class Agence extends Emprunteur{
	
	
	private String nom ;
	private List<Employe> employes = new ArrayList<Employe>();
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public List<Employe> getEmployes() {
		return employes;
	}
	public void setEmployes(List<Employe> employes) {
		this.employes = employes;
	}

	@Override
	public String toString() {
		return "Agence [nom=" + nom + ", employes=" + employes + "]";
	}
	public Agence(List<Empruntable> stock, String nom, List<Employe> employes) {
		super(stock);
		//super.ajouterAuStock(e);
		this.nom = nom;
		this.employes = employes;
	}
	public Agence(String nom) {

		this.nom = nom;
	}
	
	

}
