package ent.ensim.data;

import java.util.List;

public class Employe extends Emprunteur {
	
	private Agence agence ;
	private Entreprise entreprise ;
	
	public Agence getAgence() {
		return agence;
	}
	public void setAgence(Agence agence) {
		this.agence = agence;
	}
	public Entreprise getEntreprise() {
		return entreprise;
	}
	public void setEntreprise(Entreprise entreprise) {
		this.entreprise = entreprise;
	}
	@Override
	public String toString() {
		return "Employe [agence=" + agence + ", entreprise=" + entreprise + "]";
	}
	public Employe(List<Empruntable> stock, Agence agence, Entreprise entreprise) {
		super(stock);
		this.agence = agence;
		this.entreprise = entreprise;
	}
	public Employe(Agence agence, Entreprise entreprise) {
		this.agence = agence;
		this.entreprise = entreprise;
	}

}
