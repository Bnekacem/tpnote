package ent.ensim.data;

import java.util.ArrayList;
import java.util.List;

public class Emprunteur {
	
	
	
	 public Emprunteur() {
		super();
	}

	protected List<Empruntable> stock = new ArrayList<Empruntable>();

	public List<Empruntable> getStock() {
		return stock;
	}

	public void setStock(List<Empruntable> stock) {
		this.stock = stock;
	}

	public Emprunteur(List<Empruntable> stock) {
		super();
		this.stock = stock;
	}
	
	public void ajouterAuStock(Empruntable e) {
		stock.add(e);
		
	}
	
	public List<Empruntable> listeMateriel(){
		
		return this.stock ;
		
	}
	
	public boolean perdreMateriel(Empruntable e ) {
		
		return true ;
	}

}
