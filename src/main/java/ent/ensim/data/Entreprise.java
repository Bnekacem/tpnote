package ent.ensim.data;

import java.util.ArrayList;
import java.util.List;

public class Entreprise extends Emprunteur {

	
	 private String nomEntreprise ;
	 private List<Employe> employes = new ArrayList<Employe>();
	 private List<Agence> agence = new ArrayList<Agence>();
	 
	 
	public String getNomEntreprise() {
		return nomEntreprise;
	}
	public void setNomEntreprise(String nomEntreprise) {
		this.nomEntreprise = nomEntreprise;
	}
	public List<Employe> getEmployes() {
		return employes;
	}
	public void setEmployes(List<Employe> employes) {
		this.employes = employes;
	}
	public List<Agence> getAgence() {
		return agence;
	}
	public void setAgence(List<Agence> agence) {
		this.agence = agence;
	}
	public Entreprise(List<Empruntable> stock, String nomEntreprise, List<Employe> employes, List<Agence> agence) {
		super(stock);
		this.nomEntreprise = nomEntreprise;
		this.employes = employes;
		this.agence = agence;
	}
	public Entreprise(String nomEntreprise) {
		this.nomEntreprise = nomEntreprise;

	}
	
}
